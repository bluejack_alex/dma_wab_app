# README #

Title: DMA Web App Builder Application (Dev)
Created by: BlueJack Consulting Inc.
Date: July 24, 2018

### What is this repository for? ###

* This is the Web App Builder application for the DMA WAB App (Dev).

### How do I get set up? ###

* Created using WAB 2.8. ESRI AppBuilder Generator + ESRI Widget Generator + Gulp was used for dynamic widget editing. 
* Seperate Repo will exist containing strictly widget content.
* To deploy - should just be able to paste the contents of this repo into ..\arcgis-web-appbuilder-2.8\WebAppBuilderForArcGIS\server\apps\<APPNUMBER> folder.

### Who do I talk to? ###

* Repo owner: Alexander Wojcik (awojcik@bluejack.ca / alexdouglaswojcik@gmail.com)
* Other community or team contact: Rachel O'Neil (roneil@bluejack.ca)